#!/bin/bash
# One liner for getting the directory the script resides in.
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

sudo pacman -S plasma
rm -r ~/.config/autostart-scripts
rm -r ~/.config/plasma-workspace/env
ln -s $SCRIPT_DIR/autostart-scripts ~/.config/autostart-scripts
ln -s $SCRIPT_DIR/env ~/.config/plasma-workspace/env
echo "KDE setup complete."
